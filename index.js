if('serviceWorker' in navigator){
    window.addEventListener('load', async ()=>{
        try{
            const registration = await navigator.serviceWorker.register('sw.js');
            console.log('Service Worker registered with scope:', registration.scope);
        }
        catch(err){
            console.log('Error registering Service Worker', err);
        }
    });
}