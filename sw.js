const CACHE_NAME = 'SITE_CACHE_V1';

const CACHE_URLS = [
    '/css/all.css',
    '/css/style.css',
    '/css/weather-icons.min.css',
    '/favicon/favicon.ico',
    '/favicon/site.webmanifest',
    '/js/main.js',
    '/img/wlogo.svg',
    '/img/offline.svg',
    '/index.html',
    'index.js',
    '404.html',
    'offline.html',
    'https://api.openweathermap.org/data/2.5/forecast?q=kampala&appid=9caf7981180ec411fb4419fe91abe494'
];

self.addEventListener('install', installer  => {

    const done = async () => {
        const cache = await caches.open(CACHE_NAME);
        return cache.addAll(CACHE_URLS);
    };

    installer.waitUntil(done());
});

self.addEventListener('fetch', fetchEvent => {
    // respond to fetch request with a match from the cache
    // if not in cache, then request from network and cache
    // if there is a server error, show the offline page
    const url = fetchEvent.request.url;


    const getResponse = async (request) => {
        let response;

        response = await caches.match(request);
        if(response && response.status === 200) {
            return response;
        }

        try {
            response = await fetch(request);
            if(response && response.status === 404) {
                return caches.match('404.html');
            }
        }
        catch (e) {
            return caches.match('offline.html');
        }

        const clone = response.clone();
        const cache = await caches.open(CACHE_NAME);
        cache.put(url, clone);
        return response;
    };

    fetchEvent.respondWith(getResponse(fetchEvent.request));
});

self.addEventListener('activate', activator => {

    const currentCaches = [CACHE_NAME];
    const done = async () => {
        const names = await caches.keys();
        return Promise.all(names.map(name => {
            if(!currentCaches.includes(name)) {
                return caches.delete(name);
            }
        }));
    };

    activator.waitUntil(done());
});
