const days = ['Sun', 'Mon', 'Tue', 'Wed', 'Thur', 'Fri', 'Sat']
const months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
const button = document.getElementById("search-button");
const query = document.getElementById("search");

function updateui(_data){
    let data = _data.data;
    let city = _data.city;
    let icon = document.getElementsByClassName("wi");
    let day = document.getElementsByClassName("day");
    let desc = document.getElementsByClassName("desc");
    let max = document.getElementsByClassName("max");
    let min = document.getElementsByClassName("min");
    let time = document.getElementById('time');
    let value = document.getElementsByClassName('value');
    let display_city = document.getElementById('city');
    console.log(data);
    data.forEach((element, index) => {
        icon[index].classList.replace(icon[index].classList[2], `wi-owm-${element.weather[0].id}`);
        desc[index].classList.add('capitalize');
        desc[index].innerHTML=`${element.weather[0].description}<br> ${(element.pop*100).toFixed()}% of Rain`;
        max[index].innerHTML=`${element.main.temp_max.toFixed()}&degF`;
        min[index].innerHTML=`${element.main.temp_min.toFixed()}&degF`;
        if(index===0){
            let date = new Date(element.dt_txt);
            display_city.innerHTML=city.name;
            time.innerHTML = `Today:<br>${days[date.getDay()]} ${months[date.getMonth()]} ${date.getDate()} ${date.getFullYear()}`;
            day[index].innerHTML='Now';
            for(let i=0; i<value.length;i++){
                if(i===0){
                value[i].innerHTML=`${element.main.humidity}%`;
                }
                else if(i===1){
                    value[i].innerHTML=`${element.wind.speed} m/s`;

                }else{
                    value[i].innerHTML=`${element.main.pressure} hPa`;
                }
            }
        }else{
            day[index].innerHTML= days[new Date(element.dt_txt).getDay()];
        }

    });
}

function snackbar(){
    let bar = document.getElementById('snackbar');
    bar.innerHTML='Could not Find the City.'
    // Add the "show" class to DIV
    bar.className = "show";

  // After 3 seconds, remove the show class from DIV
    setTimeout(function(){ bar.className = bar.className.replace("show", ""); }, 3000);
    }

function owm(city){
    city = city||'kampala';
    city = city.trim();
    let apikey="9caf7981180ec411fb4419fe91abe494";
    let url=`https://api.openweathermap.org/data/2.5/forecast?q=${city}&appid=${apikey}`;
    return function(){
        fetch(url).then(resp=>resp.json()).then(data=>{
            if(data.cod==='404'){
                snackbar();
            }
            return {data:data.list.filter(entry=>entry.dt_txt.slice(11)===data.list[0].dt_txt.slice(11)),
                    city:data.city}
            }).then(updateui)
            .catch(err=>console.log('Error in fetching data:',err));
    }
}

owm()();

button.addEventListener('click',event=>{
    if(query.value!==""){
        owm(query.value)();
        query.value="";
    }
});

